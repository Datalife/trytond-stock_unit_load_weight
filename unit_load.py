# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id, Equal, And, Bool, If, Not, In, Len
from trytond.transaction import Transaction
from trytond.wizard import (Wizard, StateView, Button, StateTransition,
        StateAction, StateReport)
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.tools import cached_property


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    WEIGHT_REQ = And(
        Not(In(Eval('state'), ['draft', 'cancelled'])),
        And(Bool(Eval('weight_required')),
            Eval('production_state') == 'running'))

    weight = fields.Function(fields.Float('Weight',
        digits=(16, Eval('weight_unit_digits', 2)),
        depends=['weight_unit_digits']), 'get_weight')
    net_weight = fields.Function(fields.Float('Net weight',
        digits=(16, Eval('weight_unit_digits', 2)),
        states={
            'readonly': (Equal(
                Eval('uom_category', 0), Id('product', 'uom_cat_weight')) & ((
                    Eval('production_state') != 'running') | (
                    Eval('state') != 'draft'))
                ) | (
                    (Eval('production_state') != 'running') &
                    ~Eval('available')
                ),
            'invisible': Equal(Eval('uom_category', 0),
                Id('product', 'uom_cat_weight')),
            'required': WEIGHT_REQ},
        depends=['weight_unit_digits', 'production_state', 'uom_category',
            'weight_required', 'available', 'state']),
        'get_net_weight')
    tare = fields.Float('Tare', digits=(16, Eval('weight_unit_digits', 2)),
        states={
            'readonly': (Equal(
                Eval('uom_category', 0), Id('product', 'uom_cat_weight')) & ((
                    Eval('production_state') != 'running') | (
                    Eval('state') != 'draft'))
                ) | (
                    (Eval('production_state') != 'running') &
                    ~Eval('available')
                ),
            'required': WEIGHT_REQ},
        domain=[
            If(Eval('tare'),
                ('tare', '>=', 0),
                ()
            ),
            If(Eval('tare') & Eval('gross_weight'),
                ('tare', '<=', Eval('gross_weight')),
                ()
            )],
        depends=['weight_unit_digits', 'production_state', 'weight_required',
            'available', 'uom_category', 'state', 'tare', 'gross_weight'])
    gross_weight = fields.Float('Gross weight',
        digits=(16, Eval('weight_unit_digits', 2)),
        states={
            'readonly': (Equal(
                Eval('uom_category', 0), Id('product', 'uom_cat_weight')) & ((
                    Eval('production_state') != 'running') | (
                    Eval('state') != 'draft'))
                ) | (
                    (Eval('production_state') != 'running') &
                    ~Eval('available')
                ),
            'required': WEIGHT_REQ},
        domain=[
            If(Eval('gross_weight'),
                ('gross_weight', '>=', 0),
                ()
            ),
            If(Eval('tare') & Eval('gross_weight'),
                ('gross_weight', '>=', Eval('tare')),
                ()
            )],
        depends=['weight_unit_digits', 'production_state', 'available',
            'weight_required', 'uom_category', 'state', 'tare',
            'gross_weight'])
    weight_unit = fields.Many2One('product.uom', 'Weight UOM',
        domain=[('category', '=', Id('product', 'uom_cat_weight'))],
        states={
            'readonly': (Equal(
                Eval('uom_category', 0), Id('product', 'uom_cat_weight')) & (
                    Eval('production_state') != 'draft')
                ) | (
                    (Eval('production_state') != 'running') &
                    ~Eval('available')
                ),
            'invisible': Equal(Eval('uom_category', 0),
                Id('product', 'uom_cat_weight'))},
        depends=['uom_category', 'production_state', 'available'])
    weight_unit_digits = fields.Function(fields.Integer('Weight UOM digits'),
        'on_change_with_weight_unit_digits')
    weight_required = fields.Function(fields.Boolean('Weight required'),
        'on_change_with_weight_required')
    case_weight = fields.Function(
        fields.Float('Case weight',
            digits=(16, Eval('weight_unit_digits', 2)),
            depends=['weight_unit_digits']),
        'get_case_weight')
    unit_weight = fields.Function(
        fields.Float('Unit weight',
            digits=(16, Eval('weight_unit_digits', 2)),
            depends=['weight_unit_digits']),
        'get_unit_weight')

    @classmethod
    def __setup__(cls):
        super(UnitLoad, cls).__setup__()
        cls._buttons.update({
            'weigh': {
                'icon': 'tryton-lauch',
                'invisible': ~Eval('available'),
                'depends': ['available']
            }
        })
        cls._deny_modify_not_available |= {'tare', 'gross_weight'}
        cls._deny_modify_done |= {'tare', 'gross_weight'}

    @classmethod
    def get_weight(cls, records, name):
        return {r.id: r.net_weight for r in records}

    @classmethod
    def get_net_weight(cls, records, name):
        res = {}
        for record in records:
            if record._must_match_weight_quantity():
                res[record.id] = record.quantity
            elif record.gross_weight is not None and record.tare is not None:
                unit = record.weight_unit
                res[record.id] = record.gross_weight - record.tare
                if unit:
                    res[record.id] = record.weight_unit.round(res[record.id])
            else:
                res[record.id] = None
        return res

    @classmethod
    def default_weight_unit(cls):
        pool = Pool()
        Configuration = pool.get('stock.configuration')

        conf = Configuration(1)
        if conf.ul_weight_uom:
            return conf.ul_weight_uom.id
        return None

    @classmethod
    def default_weight_unit_digits(cls):
        pool = Pool()
        Uom = pool.get('product.uom')

        uom_id = cls.default_weight_unit()
        if uom_id:
            return Uom(uom_id).digits
        return 2

    @staticmethod
    def default_weight_required():
        return False

    def on_change_with_weight_required(self, name=None):
        return False

    @fields.depends('weight_unit', 'uom_category')
    def on_change_with_weight_unit_digits(self, name=None):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')

        cat_id = Modeldata.get_id('product', 'uom_cat_weight')
        if self.weight_unit:
            return self.weight_unit.digits
        elif self.uom_category and self.uom_category.id == cat_id:
            return self.uom.digits
        return 2

    @fields.depends(methods=['_compute_net_weight'])
    def on_change_gross_weight(self):
        self._compute_net_weight()

    @fields.depends(methods=['_compute_gross_weight'])
    def on_change_net_weight(self):
        self._compute_gross_weight()

    @fields.depends(methods=['_compute_net_weight'])
    def on_change_tare(self):
        self._compute_net_weight()
        self._compute_gross_weight()

    @fields.depends('gross_weight', 'tare', 'uom', 'weight_unit',
        methods=['_must_match_weight_quantity', 'on_change_quantity'])
    def _compute_net_weight(self):
        if self.gross_weight and self.tare is not None and self.weight_unit:
            self.net_weight = self.gross_weight - self.tare
            if self._must_match_weight_quantity():
                self.quantity = self.uom.round(self.net_weight)
                self.on_change_quantity()

    @fields.depends('net_weight', 'gross_weight', 'tare', 'weight_unit')
    def _compute_gross_weight(self):
        if self.net_weight and self.tare and self.weight_unit and \
                self.gross_weight is None:
            self.gross_weight = self.weight_unit.round(
                self.net_weight + self.tare)

    @fields.depends(methods=['_must_match_weight_quantity', '_set_weight'])
    def on_change_quantity(self):
        super(UnitLoad, self).on_change_quantity()
        self._set_weight(set_tare=not self._must_match_weight_quantity())

    @fields.depends(methods=['_set_weight'])
    def on_change_cases_quantity(self):
        super(UnitLoad, self).on_change_cases_quantity()
        self._set_weight()

    @fields.depends('product', 'uom', 'net_weight', 'gross_weight', 'tare',
            'quantity', methods=[
                '_must_match_weight_quantity',
                'on_change_with_weight_required'
            ])
    def _set_weight(self, set_tare=True,
            names=['net_weight', 'gross_weight']):
        if not self.product:
            return
        uom = getattr(self, 'uom', None) or self.product.default_uom
        if not self.quantity or not uom:
            return

        if not self._must_match_weight_quantity():
            return

        _update_gross_weight = False
        _required = self.on_change_with_weight_required()
        if 'net_weight' in names and self.net_weight != self.quantity:
            self.net_weight = self.quantity
            _update_gross_weight = True
        if ('gross_weight' in names and self.tare is not None and (
                (self.gross_weight is None or _update_gross_weight) or
                _required)):
            self.gross_weight = uom.round(
                self.net_weight + self.tare)

    def _set_tare(self):
        pass

    @classmethod
    def check_required_weight(cls, records):
        for record in records:
            if (record.weight_required and (
                    record.gross_weight is None or
                    record.net_weight is None or
                    record.tare is None)):
                raise UserError(gettext(
                    'stock_unit_load_weight.msg_stock_unit_load_required_weight',
                    unit_load=record.rec_name))

    @classmethod
    def assign(cls, records):
        cls.check_required_weight(records)
        cls.check_weight(records)
        super(UnitLoad, cls).assign(records)

    @classmethod
    def do(cls, records):
        cls.check_weight(records)
        super(UnitLoad, cls).do(records)

    @classmethod
    def check_weight(cls, records, force=False):
        for record in records:
            if not record._check_weight:
                raise UserError(gettext(
                    'stock_unit_load_weight.msg_stock_unit_load_invalid_weight',
                    unit_load=record.rec_name))

    def _check_weight(self):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')

        cat_id = Modeldata.get_id('product', 'uom_cat_weight')
        if self.production_state != 'running':
            return True
        if self.uom_category.id != cat_id:
            return True
        if self.gross_weight is None or self.tare is None:
            return True

        net_weight = self.uom.round(self.gross_weight - self.tare)
        return net_weight == self.quantity

    @fields.depends('uom')
    def _must_match_weight_quantity(self):
        pool = Pool()
        Modeldata = pool.get('ir.model.data')

        cat_id = Modeldata.get_id('product', 'uom_cat_weight')
        return self.uom and self.uom.category.id == cat_id

    @classmethod
    def get_case_weight(cls, records, name=None):
        return {r.id: r.weight_unit.round(
            r.net_weight / r.cases_quantity) if r.net_weight and r.quantity
            else 0 for r in records}

    @classmethod
    def get_unit_weight(cls, records, name=None):
        return {r.id: r.weight_unit.round(
            r.net_weight / r.quantity) if r.net_weight and r.quantity
            else 0 for r in records}

    @classmethod
    @ModelView.button_action(
        'stock_unit_load_weight.act_wizard_stock_unit_load_weigh')
    def weigh(cls, records):
        pass


class UnitLoadWeightData(ModelView):
    '''Unit Load Weight Data'''
    __name__ = 'stock.unit_load.weigh.data'

    unit_load = fields.Many2One('stock.unit_load', 'Unit Load',
        readonly=True, states={
            'invisible': Len(Eval('unit_loads')) > 1
        },
        depends=['unit_loads'])
    gross_weight = fields.Float('Gross weight',
        digits=(16, Eval('weight_unit_digits', 2)),
        states={
            'invisible': Len(Eval('unit_loads')) > 1
        },
        depends=['weight_unit_digits', 'unit_loads'])
    tare = fields.Float('Tare', digits=(16, Eval('weight_unit_digits', 2)),
        states={
            'invisible': Len(Eval('unit_loads')) > 1
        },
        depends=['weight_unit_digits', 'unit_loads'])
    weight_unit = fields.Many2One('product.uom', 'Weight UOM',
        states={
            'invisible': Len(Eval('unit_loads')) > 1
        },
        depends=['unit_loads'])
    weight_unit_digits = fields.Integer('Weight UOM digits')
    unit_loads = fields.One2Many('stock.unit_load', None, "Unit loads",
        readonly=True, states={
            'invisible': Len(Eval('unit_loads')) <= 1
        },
        depends=['unit_loads'])

    @fields.depends('gross_weight', 'tare')
    def on_change_gross_weight(self):
        if self.gross_weight is not None and self.tare is None:
            self.tare = 0


class UnitLoadWeightStart(ModelView):
    '''Unit Load Weight Start'''
    __name__ = 'stock.unit_load.weigh.start'

    ul_code = fields.Char('Unit Load code',
        states={
            'required': ~Bool(Eval('unit_load'))
        },
        depends=['unit_load'])
    unit_load = fields.Many2One('stock.unit_load', 'Unit Load',
        domain=[
            If(Bool(Eval('ul_code')),
                ('code', '=', Eval('ul_code')), ())
        ],
        depends=['ul_code'])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.unit_load.domain.append(cls._get_unit_load_available_domain())

    @classmethod
    def _get_unit_load_available_domain(cls):
        return ('available', '=', True)

    @fields.depends('ul_code')
    def on_change_ul_code(self):
        pool = Pool()
        UnitLoad = pool.get('stock.unit_load')
        unit_load = None
        if self.ul_code:
            unit_load = UnitLoad.search(
                UnitLoad._get_barcode_search_domain(self.ul_code))
        else:
            self.unit_load = None
        if unit_load:
            self.unit_load = unit_load[0]

    @fields.depends('unit_load', 'ul_code')
    def on_change_unit_load(self):
        if not self.unit_load and self.ul_code:
            self.ul_code = None


class WeighUL(Wizard):
    '''Weight Unit Load'''
    __name__ = 'stock.unit_load.weigh'
    start_state = 'pre_start'

    pre_start = StateTransition()
    start = StateView('stock.unit_load.weigh.start',
        'stock_unit_load_weight.stock_unit_load_weigh_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Next', 'pre_data', 'tryton-forward', default=True)])
    pre_data = StateTransition()
    data = StateView('stock.unit_load.weigh.data',
        'stock_unit_load_weight.stock_unit_load_weigh_view_data_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Back', 'start', 'tryton-back', states={
                'invisible': Eval('context', {}
                    ).get('active_model') == 'stock.unit_load'
                }),
            Button('Do', 'do_', 'tryton-ok', default=True),
            Button('Do and back', 'open_', 'tryton-open', states={
                'invisible': Eval('context', {}
                    ).get('active_model') == 'stock.unit_load'
                }),
            Button('Do and print', 'print_', 'tryton-print')])
    do_ = StateTransition()
    open_ = StateAction('stock_unit_load.act_unit_load')
    print_ = StateReport('stock.unit_load.label')

    def transition_pre_start(self):
        Unitload = Pool().get('stock.unit_load')
        if Transaction().context.get('active_model') == 'stock.unit_load':
            self.start.unit_load = Unitload(Transaction().context['active_id'])
            return self.transition_pre_data()
        return 'start'

    def check_ul_not_available(self):
        if not self.start.unit_load.available and \
                self.start.unit_load.production_state != 'running':
            raise UserError(gettext(
                'stock_unit_load.msg_stock_unit_load_ul_not_available',
                unit_load=self.start.unit_load.rec_name))

    def transition_pre_data(self):
        if not self.start.unit_load:
            raise UserError(gettext(
                'stock_unit_load_weight.msg_stock_unit_load_weigh_no_ul_found',
                code=self.start.ul_code))
        self.check_ul_not_available()
        return 'data'

    def default_data(self, fields):
        res = {
            'unit_load': self.start.unit_load.id,
            'gross_weight': self.start.unit_load.gross_weight,
            'tare': self.start.unit_load.tare,
            'weight_unit': (self.start.unit_load.weight_unit
                and self.start.unit_load.weight_unit.id),
            'weight_unit_digits': self.start.unit_load.weight_unit_digits
        }
        if len(self.records) > 1:
            res['unit_loads'] = list(map(int, self.records))
        return res

    def weight_unit_load(self, unit_load):
        if len(self.records) > 1:
            self.model.save(self.data.unit_loads)
        else:
            if self.data.gross_weight is None or self.data.tare is None:
                raise UserError(gettext(
                    'stock_unit_load_weight.'
                    'msg_stock_unit_load_weigh_missing_data'
                ))
            unit_load.gross_weight = self.data.gross_weight
            unit_load.tare = self.data.tare
            unit_load._compute_net_weight()
            unit_load.save()

    def transition_do_(self):
        self.weight_unit_load(self.data.unit_load)
        if Transaction().context.get('active_model') == 'stock.unit_load':
            return 'end'
        return 'start'

    def do_open_(self, action):
        self.weight_unit_load(self.data.unit_load)
        action['views'].reverse()
        return action, {'res_id': [self.data.unit_load.id]}

    def do_print_(self, action):
        self.weight_unit_load(self.data.unit_load)
        return action, {
            'id': self.data.unit_load.id,
            'ids': [self.data.unit_load.id]
        }

    def transition_print_(self):
        if Transaction().context.get('active_model') == 'stock.unit_load':
            return 'end'
        return 'start'


class UnitLoad2(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._deny_modify_done -= {'tare', 'gross_weight', 'internal_quantity'}

    @classmethod
    def set_quantity(cls, records, name, value):
        cls._write_product_moves_quantity(records, value)
        super().set_quantity(records, name, value)

    @classmethod
    def _write_product_moves_quantity(cls, records, quantity):

        def _get_move_quantity(moves, quantity):
            if len(moves) == 1:
                moves[0].quantity = moves[0].uom.round(quantity)
            else:
                for move in moves:
                    old_quantity = move.unit_load.quantity
                    move.quantity = move.uom.round(
                        (move.quantity / old_quantity) * quantity)

        with Transaction().set_context(check_origin=False,
                check_shipment=False):
            product_moves = [m for ul in records for m in ul.moves
                if m.product == ul.product and ul.production_state == 'done']
            if product_moves:
                pool = Pool()
                Move = pool.get('stock.move')
                Move.cancel(product_moves)
                Move.draft(product_moves)
                _get_move_quantity(product_moves, quantity)
                Move.save(product_moves)
                Move.do(product_moves)


class UnitLoad3(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    @classmethod
    def _check_deny_modify_not_available(cls, records):
        to_check = []
        for record in records:
            if (record.production_state == 'done'
                    and not record.available
                    and not record.load_line):
                to_check.append(record)
        if to_check:
            super()._check_deny_modify_not_available(to_check)


class UnitLoadWeightStart2(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.weigh.start'

    @classmethod
    def _get_unit_load_available_domain(cls):
        return ['OR',
            super()._get_unit_load_available_domain(),
            [
                ('available', '=', False),
                ('dropped', '=', False),
                ('production_state', '=', 'done'),
                ('state', '!=', 'cancel'),
                ('load_line', '!=', None),
                ['OR',
                    ('shipment', '=', None),
                    # avoid 'out', 'out return' and 'in return'
                    ('shipment', 'like', 'stock.shipment.internal,%'),
                ]
            ]
        ]


class WeighUL2(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.weigh'

    def check_ul_not_available(self):
        if (not self.start.unit_load.available
                and not self.start.unit_load.load_line):
            super().check_ul_not_available()


class WeightULRelate(Wizard):
    __name__ = 'stock.unit_load.weigh_relate'

    start = StateAction(
        'stock_unit_load_weight.act_wizard_stock_unit_load_weigh')

    def do_start(self, action):
        uls = self.unit_loads
        if not uls:
            return
        data = {
            'ids': list(map(int, uls)),
            'id': uls[0].id,
            'model': 'stock.unit_load'
        }
        return action, data

    @cached_property
    def unit_loads(self):
        return [ul for record in self.records for ul in record.unit_loads]
