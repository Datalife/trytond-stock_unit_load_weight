# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Id
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import CompanyValueMixin


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'

    ul_weight_uom = fields.MultiValue(
        fields.Many2One('product.uom', 'UL Weight UOM',
            domain=[('category', '=', Id('product', 'uom_cat_weight'))])
    )

    @classmethod
    def default_ul_weight_uom(cls, **pattern):
        return cls.multivalue_model(
            'ul_weight_uom').default_ul_weight_uom()


class ConfigurationULWeightUOM(ModelSQL, CompanyValueMixin):
    "Stock Configuration UL Weight UOM"
    __name__ = 'stock.configuration.ul_weight_uom'

    ul_weight_uom = fields.Many2One('product.uom', 'UL Weight UOM',
            domain=[('category', '=', Id('product', 'uom_cat_weight'))])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)

        super(ConfigurationULWeightUOM, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('ul_weight_uom')
        value_names.append('ul_weight_uom')
        fields.append('company')
        migrate_property('stock.configuration',
            field_names, cls, value_names, fields=fields)

    @classmethod
    def default_ul_weight_uom(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('product', 'uom_kilogram')
        except KeyError:
            return None
