=========================
Unit Load Carrier load UL
=========================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> from proteus import Model, Wizard
    >>> today = datetime.date.today()


Install modules::

    >>> config = activate_modules(['stock_unit_load_weight',
    ...     'carrier_load_ul', 'stock_move_done2cancel'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']


Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()


Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()


Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()


Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()


Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> carrier = Carrier()
    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    >>> transport_template = Template(
    ...     name='Transport',
    ...     type='service',
    ...     list_price=Decimal(500),
    ...     cost_price=Decimal(0),
    ...     default_uom=unit)
    >>> transport_template.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.purchasable = True
    >>> carrier_product.purchase_uom = carrier_product.default_uom
    >>> carrier_product.account_category = account_category_tax
    >>> carrier_product.save()


Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> dock = wh.docks.new()
    >>> dock.name = 'Dock 1'
    >>> dock.code = 'D1'
    >>> wh.save()
    >>> storage_child = Location(name='New storage zone', type='storage')
    >>> storage_child.parent = wh.storage_location
    >>> storage_child.save()


Create carrier load::

    >>> Load = Model.get('carrier.load')
    >>> load = Load()
    >>> load.company != None
    True
    >>> load.state
    'draft'
    >>> load.date == today
    True
    >>> load.warehouse != None
    True
    >>> load.warehouse_output == load.warehouse.output_location
    True
    >>> load.dock != None
    True
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX3449'
    >>> load.save()
    >>> load.code != None
    True


Create load order::

    >>> Order = Model.get('carrier.load.order')
    >>> order = Order(load=load)
    >>> order.start_date = datetime.datetime.now() - relativedelta(minutes=20)
    >>> order.state
    'draft'
    >>> order.ul_origin_restrict
    True
    >>> order.party = customer
    >>> line = order.lines.new()
    >>> line.ul_quantity = Decimal(1)
    >>> order.save()
    >>> order.code != None
    True
    >>> order.date == load.date
    True
    >>> order.click('wait')
    >>> order.state
    'waiting'


Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config, do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> template = ul.product.template
    >>> template.salable = True
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> main_product = ul.product

Add other products to unit load::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()
    >>> template = ProductTemplate()
    >>> template.name = 'Plastic Case 30x30'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('5')
    >>> product.save()
    >>> move = ul.moves.new()
    >>> move.planned_date = today
    >>> move.product = product
    >>> move.quantity = Decimal(2)
    >>> move.from_location = ul.moves[0].from_location
    >>> move.to_location = ul.moves[0].to_location
    >>> move.currency = move.company.currency
    >>> move.unit_price = product.cost_price
    >>> ul.save()
    >>> ul.click('assign')
    >>> ul.click('do')


Load UL::

    >>> start_load = Wizard('carrier.load_uls', [])
    >>> start_load.form.load_order = order
    >>> start_load.execute('post_order')
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    >>> ul.reload()
    >>> ul.load_order != None
    True
    >>> ul.available
    False
    >>> ul.gross_weight == None
    True
    >>> ul.net_weight == None
    True
    >>> order.reload()
    >>> order.state
    'running'


Weigh unit load::

    >>> weigh_unit_load = Wizard('stock.unit_load.weigh')
    >>> weigh_unit_load.form.ul_code = ul.code
    >>> weigh_unit_load.execute('pre_data')
    >>> weigh_unit_load.form.gross_weight = Decimal('1000.0')
    >>> weigh_unit_load.form.tare = Decimal('50.0')
    >>> weigh_unit_load.execute('do_')
    >>> ul.reload()
    >>> ul.gross_weight
    1000.0
    >>> ul.tare
    50.0
    >>> ul.net_weight
    950.0


Weigh unit load with load_order in done state::

    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.execute('pre_do')
    >>> order.reload()
    >>> order.state
    'done'
    >>> weigh_unit_load = Wizard('stock.unit_load.weigh')
    >>> weigh_unit_load.form.ul_code = ul.code
    >>> weigh_unit_load.execute('pre_data')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Unit load "1" is not available. - 